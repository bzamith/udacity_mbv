import os
import sys
import pandas as pd
from datetime import datetime as dt
import string 

# Function to convert UNIX's date and time
def conv_date_time(value):
	return(dt.fromtimestamp(value).strftime('%Y-%m-%d %H:%M:%S'))

# Read and check data
data = pd.read_csv('ds_1_with_fields.csv')
data.head()
data.describe()

# Convert all date and time
data['conv_start_time'] = data['start_time'].apply(conv_date_time)
data.head()
data.describe()